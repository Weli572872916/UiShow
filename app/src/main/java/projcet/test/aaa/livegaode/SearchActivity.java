package projcet.test.aaa.livegaode;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.rec_list)
    RecyclerView recList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        initData();
    }

    BaseQuickAdapter<String, BaseViewHolder> mAdapter;

    private void initData() {
        List<String> listAdvert =new ArrayList<>();
        listAdvert.add("美食");
        listAdvert.add("酒店");
        listAdvert.add("加油站");
        listAdvert.add("公交站");
        listAdvert.add("收藏夹");
        listAdvert.add("更多");

        LinearLayoutManager ms = new LinearLayoutManager(this);

        ms.setOrientation(LinearLayoutManager.HORIZONTAL);
        recList.setLayoutManager(ms);


        Log.d("SearchActivity", "listAdvert:" + listAdvert);
//        homeRecycler2.addItemDecoration(new DividerItemDecoration(getApplicationContext(), StaggeredGridLayoutManager.VERTICAL));
        recList.setAdapter(mAdapter = new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_search, listAdvert) {
            @Override
            protected void convert(BaseViewHolder helper, String item) {
                helper.setText(R.id.tv_type, item);
            }
        });

    }

}
