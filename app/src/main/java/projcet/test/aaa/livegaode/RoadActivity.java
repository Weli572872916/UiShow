package projcet.test.aaa.livegaode;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RoadActivity extends AppCompatActivity {


    @BindView(R.id.im_search)
    ImageView imSearch;
    @BindView(R.id.top)
    RelativeLayout top;
    @BindView(R.id.bt_line)
    LinearLayout btLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_road);


        ButterKnife.bind(this);

    }



    @OnClick({R.id.top, R.id.bt_line})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.top:
                startActivity(new Intent(RoadActivity.this, SearchActivity.class));
                break;
            case R.id.bt_line:
                startActivity(new Intent(RoadActivity.this, LocationActivity.class));
                break;
        }
    }
}
