package projcet.test.aaa.livegaode;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LocationActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.bt_nav)
    public void onViewClicked() {
        startActivity( new Intent(LocationActivity.this,NavTypectivity.class));
    }
}
