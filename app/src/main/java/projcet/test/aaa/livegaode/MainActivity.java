package projcet.test.aaa.livegaode;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.car)
    LinearLayout car;
    @BindView(R.id.road)
    LinearLayout road;
    @BindView(R.id.setting)
    LinearLayout setting;
    @BindView(R.id.dingdan)
    LinearLayout dingdan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.car, R.id.road, R.id.setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.car:
                startActivity( new Intent(MainActivity.this,CarActivity.class));
                break;
            case R.id.road:
                startActivity( new Intent(MainActivity.this,RoadActivity.class));
                break;
            case R.id.setting:
                startActivity( new Intent(MainActivity.this,SettingActivity.class));
                break;
        }
    }
}
